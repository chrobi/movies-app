# Movies App

[![pipeline status](https://gitlab.com/chrobi/movies-app/badges/main/pipeline.svg)](https://gitlab.com/chrobi/movies-app/-/commits/main)

Movies application consisting of two microservices

- auth service
- movie service

Data is stored in the MongoDB.

## Description

The API Service written in Express.js that allows authorized user to create movie object based on movie title passed in the request body.

Available operations:

```
GET /movies
```

```
POST /movies
```

API documentation (OpenAPI standard):

```
GET /docs
```

## Installation

Build and run docker container using docker-compose in detached mode:

```
AUTH_SERVICE_PORT={port1} MOVIE_SERVICE_PORT={port2} JWT_SECRET={auth secret} OMDB_API_KEY={api key} docker-compose up -d --build
```

## docker-compose env variables

- AUTH_SERVICE_PORT (default 3000)
- MOVIE_SERVICE_PORT (default 2121)
- NODE_ENV (production/development)
- MONGO_URI (default: mongodb://mongodb/movie-service)
- JWT_SECRET (used both in auth and movie services)
- OMDB_API_KEY (api key to the http://www.omdbapi.com/)
